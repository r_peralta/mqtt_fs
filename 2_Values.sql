USE [FS]
GO

/****** Object:  Table [dbo].[Values]    Script Date: 8/5/2021 1:43:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Values](
	[VarName] [varchar](50) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Value] [decimal](10, 3) NULL,
	[Unit] [varchar](10) NULL
) ON [PRIMARY]
GO

