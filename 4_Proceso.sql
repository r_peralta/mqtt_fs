USE [FS]
GO

/****** Object:  Table [dbo].[Proceso]    Script Date: 8/5/2021 1:43:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Proceso](
	[Nombre] [varchar](255) NOT NULL,
	[Descripcion] [varchar](255) NOT NULL,
	[Id_Ecuacion] [int] NOT NULL
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Proceso]  WITH CHECK ADD  CONSTRAINT [FK_Proceso_Ecuacion] FOREIGN KEY([Id_Ecuacion])
REFERENCES [dbo].[Ecuacion] ([Id_Ecuacion])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[Proceso] CHECK CONSTRAINT [FK_Proceso_Ecuacion]
GO

