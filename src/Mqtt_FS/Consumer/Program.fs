﻿open System
open uPLibrary.Networking.M2Mqtt
open uPLibrary.Networking.M2Mqtt.Messages
open FSharp.Data.Sql

type sql = SqlDataProvider< Common.DatabaseProviderTypes.MSSQLSERVER, "Server=localhost;Database=FS;Trusted_Connection=True;">


let topics = [for varIndex in 0..4 -> $"Var{varIndex % 5}"]

let qosLevels = [| MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE |]

let rec subscribe(topics : List<String>, client : MqttClient) =
    match topics with
    | [] -> ()
    | t::ts -> client.Subscribe([|t|], qosLevels) |> (fun _ -> subscribe(ts, client)) |> (fun _ -> ())


let saveValue (evArgs:MqttMsgPublishEventArgs)= 
    let context = sql.GetDataContext()
    
    let msg = evArgs.Message |> System.Text.Encoding.ASCII.GetString |> System.Text.Json.JsonSerializer.Deserialize<Publisher.Values>

    let v = context.Dbo.Values.Create()
    v.VarName <- msg.VarName
    v.Date <- msg.Date
    v.Value <- msg.Value
    v.Unit <- msg.Unit

    context.SubmitUpdates()

let configureMqttClient = MqttClient("localhost") |> (fun c -> c.Connect("consumer"), subscribe(topics, c), c) |> (fun (_, _, c) -> c)

[<EntryPoint>]
let main argv =

    let client = configureMqttClient
    client.MqttMsgPublishReceived.Add(saveValue)
    Console.Read() |> (fun _ -> client.Disconnect() |> (fun _ -> 0))