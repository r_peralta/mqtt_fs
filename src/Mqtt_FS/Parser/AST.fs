﻿module AST

type identifier = string

type arithmetic = Add | Subtract | Multiply | Divide
type comparison = Eq | Ne | Lt | Gt | Le | Ge
type logical = And | Or

type measureUnit =
    | S of int
    | M of int
    | Kg of int
    | A of int
    | K of int

type value =
   | Scalar of double
   | Simple of double * measureUnit
   | Compound of double * List<measureUnit>
   | True
   | False

type expr =
   | Measure of value
   | Var of identifier
   | Neg of expr
   | Arithmetic of expr * arithmetic * expr
   | Comparison of expr * comparison * expr
   | Logical of expr * logical * expr
   | IfThenElse of expr * expr * expr

type assign =
   | Set of identifier * expr

type instruction =
   | Assign of assign

type VarLookup = System.Collections.Generic.Dictionary<identifier, value>

let Joule = [S -2; M 2; Kg 1]
let M_S = [S -1; M 1]