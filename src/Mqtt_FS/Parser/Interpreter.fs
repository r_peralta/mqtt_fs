﻿module Interpreter

open AST
open PPrinter

// Cambia el signo del exponente de una unidad de medida
// EJ: negExp -1 (S 2) = S -2
let negExp expSign unit =
    match unit with
    | S e -> S (e * expSign)
    | M e -> M (e * expSign)
    | Kg e -> Kg (e * expSign)
    | A e -> A (e * expSign)
    | K e -> K (e * expSign)

// Evalua un producto o division entre un value(AST) y un valor numerico
// EJ: scalar (/) Simple(1.0, M 3) 2.0 -1 = Simple(0.5, M -3)
let scalar op expr scalarValue expSign =
    match expr with
    | Scalar v -> op v scalarValue |> Scalar
    | Simple(v, u) -> Simple(op v scalarValue, negExp expSign u)
    | Compound(v, u) -> Compound(op v scalarValue, u |> List.map (negExp expSign))

let toValue v =
    match v with
    | true -> True
    | _ -> False

let toComarisonOp op =
    match op with
    | Eq -> (=)
    | Ne -> (<>)
    | Lt -> (<)
    | Gt -> (>)
    | Le -> (<=)
    | Ge -> (>=)

// TODO: Toda esta seccion (reduceCompound) hay que replantearla. Funciona, pero no me gusta el cómo

// Toma dos unidades y suma sus exponentes
// EJ: sumExp (S 1) (S -1) = S 0
let sumExp u1 u2 =
    match u1, u2 with
    | S e1, S e2 -> S (e1 + e2)
    | M e1, M e2 -> M (e1 + e2)
    | Kg e1, Kg e2 -> Kg (e1 + e2)
    | A e1, A e2 -> A (e1 + e2)
    | K e1, K e2 -> K (e1 + e2)

// Dada una lista de unidades filtra los segundos, suma sus exponenetes y retorna una tupla (S e, b) donde:
// e es la suma de los exponentes
// b es un bool que determina si e = 0
// EJ: sumExpS [S -2; M 2; S 1; Kg 2; S 3] = S 2
let sumExpS us = (List.filter (function | S _ -> true | _ -> false) us) |> (function | [] -> (S 0, false) | l -> List.reduce sumExp l |> (function | S 0 -> (S 0, false) | u -> (u, true)))
let sumExpM us = (List.filter (function | M _ -> true | _ -> false) us) |> (function | [] -> (M 0, false) | l -> List.reduce sumExp l |> (function | M 0 -> (M 0, false) | u -> (u, true)))
let sumExpKg us = (List.filter (function | Kg _ -> true | _ -> false) us) |> (function | [] -> (Kg 0, false) | l -> List.reduce sumExp l |> (function | Kg 0 -> (Kg 0, false) | u -> (u, true)))
let sumExpA us = (List.filter (function | A _ -> true | _ -> false) us) |> (function | [] -> (A 0, false) | l -> List.reduce sumExp l |> (function | A 0 -> (A 0, false) | u -> (u, true)))
let sumExpK us = (List.filter (function | K _ -> true | _ -> false) us) |> (function | [] -> (K 0, false) | l -> List.reduce sumExp l |> (function | K 0 -> (K 0, false) | u -> (u, true)))

// Recibe un valor y una lista de unidades. Reduce la lista de unidades al minimo value(AST) posible
// EJS: reduceCompound 10.0 [S -2; M 2; S 1; Kg 2; S 3] = Compound(10.0, [S 2; M 2; Kg 2])
//      reduceCompound 10.0 [S -2; S 4] = Simple(10.0, S 2)
//      reduceCompound 10.0 [S -2; S 2] = Scalar 10.0
let reduceCompound v lu = 
    let result = List.filter (function | (_, v) -> v) [sumExpS lu; sumExpM lu; sumExpKg lu; sumExpA lu; sumExpK lu]
    match result with
    | [] -> Scalar v
    | [(u, _)] -> Simple(v, u)
    | us -> Compound(v, us |> List.map (fun (u, _) -> u))

// 


let rec evalExpr state (expr:expr) =
    let (vars:VarLookup) = state
    match expr with
    | Measure(Compound(v, u)) -> reduceCompound v u
    | Measure v -> v
    | Var identifier -> vars.[identifier]
    | Neg v -> arithmetic (evalExpr state v) Multiply (Scalar -1.0)
    | Arithmetic(l, op, r) -> arithmetic (evalExpr state l) op (evalExpr state r)
    | Comparison(l,op,r) -> compareArithmetic (evalExpr state l) op (evalExpr state r)
    | Logical(l,op,r) -> compareLogic (evalExpr state l) op (evalExpr state r)
    | IfThenElse(c, v1, v2) -> if (evalExpr state c) = True then evalExpr state v1 else evalExpr state v2
and arithmetic lhs op rhs =
    match op, (lhs, rhs) with
    | Add, (Scalar l, Scalar r) -> Scalar(l + r)
    | Add, (Simple(v1, u1), Simple(v2, u2)) when u1 = u2 -> Simple(v1+v2, u1)
    | Add, (Compound(v1, lu1), Compound(v2, lu2)) when lu1 |> List.forall (fun e1 -> lu2 |> List.exists (fun e2 -> e1 = e2)) -> Compound(v1+v2, lu1)
    | Add, (Scalar _, vu) | Add, (vu, Scalar _) -> raise (System.Exception($"Error de tipos: No se puede sumar un valor sin unidad con {vu |> ppUnit}"))
    | Add, (l, r) -> raise (System.Exception($"Error de tipos: No se puede sumar {l |> ppUnit} con {r |> ppUnit}"))

    | Subtract, (Scalar l, Scalar r) -> Scalar(l - r)
    | Subtract, (Simple(v1, u1), Simple(v2, u2)) when u1 = u2 -> Simple(v1-v2, u1)
    | Subtract, (Compound(v1, lu1), Compound(v2, lu2)) when lu1 |> List.forall (fun e1 -> lu2 |> List.exists (fun e2 -> e1 = e2)) -> Compound(v1-v2, lu1)
    | Subtract, (Scalar _, vu) | Add, (vu, Scalar _) -> raise (System.Exception($"Error de tipos: No se puede restar un valor sin unidad con {vu |> ppUnit}"))
    | Subtract, (l, r) -> raise (System.Exception($"Error de tipos: No se puede restar {l |> ppUnit} con {r |> ppUnit}"))

    | Multiply, (expr, Scalar v) | Multiply, (Scalar v, expr) -> scalar (*) expr v 1
    | Multiply, (Simple(v1, u1), Simple(v2, u2)) -> reduceCompound (v1 * v2) [u1; u2]
    | Multiply, (Compound(v1, lu1), Compound(v2, lu2)) -> reduceCompound (v1 * v2) (lu1 @ lu2)
    | Multiply, (Simple(v1, u1), Compound(v2, lu2)) -> reduceCompound (v1 * v2) (u1::lu2)
    | Multiply, (Compound(v1, lu1), Simple(v2, u2)) -> reduceCompound (v1 * v2) (u2::lu1)
    
    | Divide, (expr, Scalar v) -> scalar (/) expr v 1
    | Divide, (Scalar v, expr) -> scalar (fun x y -> y / x) expr v -1
    | Divide, (Simple(v1, u1), Simple(v2, u2)) -> reduceCompound (v1 / v2) [u1; u2 |> negExp -1]
    | Divide, (Compound(v1, lu1), Compound(v2, lu2)) -> reduceCompound (v1 / v2) (lu1 @ (lu2 |> List.map (negExp -1)))
    | Divide, (Simple(v1, u1), Compound(v2, lu2)) -> reduceCompound (v1 / v2) (u1::(lu2 |> List.map (negExp -1)))
    | Divide, (Compound(v1, lu1), Simple(v2, u2)) -> reduceCompound (v1 / v2) ((u2 |> negExp -1)::lu1)
    
    | _ -> raise (System.Exception("Error de tipos"))
and compareArithmetic v1 op v2 = 
    match op, v1, v2 with 
    | op, Scalar v1, Scalar v2 -> (toComarisonOp op v1 v2) |> toValue
    | op, Simple(v1, u1), Simple(v2, u2) when u1 = u2 -> (toComarisonOp op v1 v2) |> toValue
    | op, Compound(v1, lu1), Compound(v2, lu2) when lu1 |> List.forall (fun e1 -> lu2 |> List.exists (fun e2 -> e1 = e2)) -> (toComarisonOp op v1 v2) |> toValue
    | op, Scalar _, vu | op, vu, Scalar _ -> raise (System.Exception($"Error de tipos: No se puede realizar la comparación {op |> ppComarisonOp} entre un valor sin unidad con {vu |> ppUnit}"))
    | op, v1, v2 -> raise (System.Exception($"Error de tipos: No se puede realizar la comparación {op |> ppComarisonOp} entre {v1 |> ppUnit} con {v2 |> ppUnit}"))
and compareLogic v1 op v2 = 
    match op, v1, v2 with
    | And, True, True -> True
    | And, _, _ -> False
    
    | Or, False, False -> False
    | Or, _, _ -> True

// Punto de entrada al evaluador. Recibe una lista de asignaciones y las resuelve, retornando un diccionario con los valores de cada asignacion.
let run (program:instruction[]) =
    let pi = ref 0
    let variables = VarLookup()   
    let eval = evalExpr variables
    let assign (Set(identifier,expr)) = variables.[identifier] <- eval expr    
    let step () =
        let instruction = program.[!pi]
        match instruction with
        | Assign(set) -> assign set
    while !pi < program.Length do step (); incr pi
    variables