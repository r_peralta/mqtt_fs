﻿module PPrinter

open AST
open System

let ppUnit u =
    match u with
    | Simple(_, S 1) -> $"<s>"
    | Simple(_, S e) -> $"<s^{e}>"
    | Simple(_, M 1) -> $"<m>"
    | Simple(_, M e) -> $"<m^{e}>"
    | Simple(_, Kg 1) -> $"<kg>"
    | Simple(_, Kg e) -> $"<kg^{e}>"
    | Simple(_, A 1) -> $"<A>"
    | Simple(_, A e) -> $"<A^{e}>"
    | Simple(_, K 1) -> $"<K>"
    | Simple(_, K e) -> $"<K^{e}>"
    | Compound(_, lu) when lu = Joule -> $"<J>"
    | Compound(_, lu) when lu = M_S -> $"<m/s>"
    | Compound(_, lu) -> $"<{lu}>"

let ppValueUnit u =
    match u with
    | Scalar x -> x.ToString()
    | Simple(v, u) -> $"{v}{Simple(v, u) |> ppUnit}"
    | Compound(v, lu) -> $"{v}{Compound(v, lu) |> ppUnit}"

let ppComarisonOp op =
    match op with
    | Eq -> "="
    | Ne -> "<>"
    | Lt -> "<"
    | Gt -> ">"
    | Le -> "<="
    | Ge -> ">="

let printDict (dict:VarLookup) = [ for entry in dict -> entry.Key + ": " + (entry.Value |> ppValueUnit) + "\n" ]
