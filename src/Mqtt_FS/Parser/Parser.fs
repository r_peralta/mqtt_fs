﻿module Parser

open AST
open FParsec

// Parsea espacios en blanco. Retorna ()
let ws = skipManySatisfy (fun c -> c = ' ' || c = '\t' || c='\r')
// Parsea un string seguido de espacios en blanco. Retorna el string parseado
let str_ws s = pstring s .>> ws

// Parser de operadores aritmeticos. Retorna una expr que representa la operación parseada
// Los terminos posibles son definidos mas abajo, dado que requieren de parsers que poseen recursion mutua con este
let oppa = new OperatorPrecedenceParser<expr,unit,unit>()
let parithmetic = oppa.ExpressionParser
oppa.AddOperator(InfixOperator("+", ws, 1, Associativity.Left, fun x y -> Arithmetic(x, Add, y)))
oppa.AddOperator(InfixOperator("-", ws, 1, Associativity.Left, fun x y -> Arithmetic(x, Subtract, y)))
oppa.AddOperator(InfixOperator("*", ws, 2, Associativity.Left, fun x y -> Arithmetic(x, Multiply, y)))
oppa.AddOperator(InfixOperator("/", ws, 2, Associativity.Left, fun x y -> Arithmetic(x, Divide, y)))
oppa.AddOperator(PrefixOperator("-", ws, 2, true, fun x -> Neg(x)))

// Parser de operadores de comparación. Retorna una expr que representa la operación parseada
let oppc = new OperatorPrecedenceParser<expr,unit,unit>()
let pcomparison = oppc.ExpressionParser
let termc = (parithmetic .>> ws) <|> between (str_ws "(") (str_ws ")") pcomparison
oppc.TermParser <- termc
oppc.AddOperator(InfixOperator("=", ws, 1, Associativity.None, fun x y -> Comparison(x, Eq, y)))
oppc.AddOperator(InfixOperator("<>", ws, 1, Associativity.None, fun x y -> Comparison(x, Ne, y)))
oppc.AddOperator(InfixOperator("<=", ws, 2, Associativity.None, fun x y -> Comparison(x, Le, y)))
oppc.AddOperator(InfixOperator(">=", ws, 2, Associativity.None, fun x y -> Comparison(x, Ge, y)))
oppc.AddOperator(InfixOperator("<", ws, 2, Associativity.None, fun x y -> Comparison(x, Lt, y)))
oppc.AddOperator(InfixOperator(">", ws, 2, Associativity.None, fun x y -> Comparison(x, Gt, y)))

// Parser de operadores lógicos. Retorna una expr que representa la operación parseada
let oppl = new OperatorPrecedenceParser<expr,unit,unit>()
let plogical = oppl.ExpressionParser
let terml = (pcomparison .>> ws) <|> between (str_ws "(") (str_ws ")") plogical
oppl.TermParser <- terml
oppl.AddOperator(InfixOperator("And", ws, 1, Associativity.Left, fun x y -> Logical(x,And,y)))
oppl.AddOperator(InfixOperator("Or", ws, 1, Associativity.Left, fun x y -> Logical(x,Or,y)))

//Parser genérico de unidades. Recibe el identificador de la unidad a parsear como un string, el ctor de la unidad en el AST y si la unidad permite exponente.
let punit mu muCtor allowExp = 
    if allowExp then pipe4 (stringReturn mu muCtor) (skipString "^" <|>% ()) (stringReturn "-" (-1) <|>% (1)) (pint32 <|>% 1) (fun muCtor _ sign cant -> muCtor (sign * cant))
    else stringReturn mu (muCtor 1)

// EJ: Los siguientes parsers estan construidos en base a punit
let psecond = punit "s" (fun e -> fun v -> Simple(v, S e)) true
let pmetre = punit "m" (fun e -> fun v -> Simple(v, M e)) true
let pkilogram = punit "kg" (fun e -> fun v -> Simple(v, Kg e)) true
let pampere = punit "A" (fun e -> fun v -> Simple(v, A e)) true
let pkelvin = punit "K" (fun e -> fun v -> Simple(v, K e)) true

let phour = punit "h" (fun e -> fun v -> Simple(v * 3600.0 ** float e, S e)) true
let pminute = punit "min" (fun e -> fun v -> Simple(v * 60.0 ** float e, S e)) true
let pcm = punit "cm" (fun e -> fun v -> Simple(v / 100.0 ** float e, M e)) true
let pkm = punit "km" (fun e -> fun v -> Simple(v * 1000.0 ** float e, M e)) true
let pgram = punit "g" (fun e -> fun v -> Simple(v / 1000.0 ** float e, Kg e)) true
let ptn = punit "Tn" (fun e -> fun v -> Simple(v * 1000.0 ** float e, Kg e)) true
let pcelsius = punit "°C" (fun e -> fun v -> Simple(v + 273.15, K e)) false

let pkwh = punit "J" (fun _ -> fun v -> Compound(v, Joule)) false

let pscalar = stringReturn "" Scalar
// Combinacion de los parsers de unidades.
let pmeasureUnit = 
    choice [ 
        pkwh; //Si este parse se ubica despues del de Kelvin falla el parseo, habria que mapear un attempt a la lista y probar si eso lo resuelve

        psecond; pminute; phour;
        pmetre; pcm; pkm;
        pkilogram; pgram; ptn;
        pampere;
        pkelvin; pcelsius;
    ]
// Parser de tipo. Parsea una unidad entre angle brakets o un string vacio(scalar)
let ptype = (between (str_ws "<") (str_ws ">") pmeasureUnit) <|> pscalar
// Parser de valor con tipo. Parsea un numero (entero o con parte decimal), seguido de un tipo
// EJ = pmeasureValue "10.0<Kg>" = Measure (Simple(10.0, Kg 1))
let pmeasureValue = pfloat .>>. ptype |>> (fun (nl, u) -> Measure(u (float nl)))


let pidentifier =
    let isIdentifierFirstChar c = isLetter c || c = '_'
    let isIdentifierChar c = isLetter c || isDigit c || c = '_'
    many1Satisfy2L isIdentifierFirstChar isIdentifierChar "identifier"
let pidentifier_ws = pidentifier .>> ws
let pvar = pidentifier |>> (fun x -> Var(x))

let pif = str_ws "SI" >>. between (str_ws "(") (str_ws ")") (pipe3 (plogical <|> pcomparison) (str_ws ";" >>. parithmetic) (str_ws ";" >>. parithmetic) (fun cond v1 v2 -> IfThenElse(cond, v1, v2)))

let pvalue = 
    choice [
        pif;  //El if si o si se tiene que parsear antes q las variables, dado que las variables contemplan la sintaxis del if ("SI")
        pvar;
        pmeasureValue;
    ]

// Definicion de los terminos del parser de expresiones aritmeticas
let terma = (pvalue .>> ws) <|> between (str_ws "(") (str_ws ")") parithmetic
oppa.TermParser <- terma
 
let passign = pipe3 pidentifier_ws (str_ws "=") parithmetic (fun id _ e -> Assign(Set(id, e)))

type Line = Blank | Instruction of instruction
let pcomment = pchar '\'' >>. skipManySatisfy (fun c -> c <> '\n') >>. pchar '\n'
let peol = pcomment <|> (pchar '\n')
let pinstruction = ws >>. passign .>> peol |>> (fun i -> Instruction i)
let pblank = ws >>. peol |>> (fun _ -> Blank)
let plines = many (pinstruction <|> pblank) .>> eof
let parse (program:string) =    
    match run plines program with
    | Success(result, _, _)   -> 
        result 
        |> List.choose (function Instruction i -> Some i | Blank -> None) 
        |> List.toArray
    | Failure(errorMsg, e, s) -> failwith errorMsg