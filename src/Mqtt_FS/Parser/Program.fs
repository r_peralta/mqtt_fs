﻿open Parser
open Interpreter
open PPrinter
open FSharp.Data.Sql
open System.Linq
open System

type sql = SqlDataProvider< Common.DatabaseProviderTypes.MSSQLSERVER, "Server=localhost;Database=FS;Trusted_Connection=True;">

[<EntryPoint>]
let main _ =
    let ctx = sql.GetDataContext()
    
    // Se obtiene un string con cada ecuacion de cada proceso por linea
    let inputKpis = ctx.Dbo.Proceso.Select(fun p -> p.``dbo.Ecuacion by Id_Ecuacion``.Select(fun e -> e.Formula) |> Seq.toList |> List.fold (fun s1 s2 -> s1 + "\n" + s2 + "\n") "") |> Seq.toList |> List.fold (fun s1 s2 -> s1 + s2) ""
    // Se obtiene una lista de tuplas (fechaHora, valoresVariables)
    let inputVars = ctx.Dbo.Values |> Seq.toList |> (fun x -> x.GroupBy(fun v -> v.Date).Select(fun g -> (g.Key, g.Select(fun vg -> $"{vg.VarName} = {vg.Value}{vg.Unit}\n") |> Seq.toList)) |> Seq.toList |> List.map (fun (fh, v) -> (fh, v |> List.fold (fun s1 s2 -> s1 + s2) "")))
    
    // Se generan los ASTs de las ecuaciones de los procesos
    let expressionsKpis = parse inputKpis
    
    // Por cada tupla (fechaHora, valoresVariables) se genera una nueva tupla de la forma (fechaHora, valorizacionFormulasProcesos), 
    //donde 'valorizacionFormulasProcesos' son los valores de las formula de cada proceso evaluada por fechaHora con los valoresVariables
    let results = inputVars |> List.map (fun (fh, v) -> (fh, Array.concat [|parse v; expressionsKpis|] |> run |> printDict))
    
    // Se imprimen fechaHora y los valores de los KPIS de los procesos
    results |> List.map (fun (fh, r) -> $"""{fh}:{"\n"}{r |> List.filter (fun ast -> ast.Contains("KPI")) |> List.fold (fun s1 s2 -> s1 + s2) ""}""" |> Console.WriteLine) |> ignore
    
    0