﻿namespace Publisher.Controllers

open System
open System.Collections.Generic
open System.Linq
open System.Threading.Tasks
open Microsoft.AspNetCore.Mvc
open Microsoft.Extensions.Logging
open Publisher
open uPLibrary.Networking.M2Mqtt

[<ApiController>]
[<Route("[controller]")>]
type ValuesController (logger : ILogger<ValuesController>) =
    inherit ControllerBase()

    [<HttpGet>]
    member _.Get() =
        let client = MqttClient("localhost") 
        client.Connect("publisher") |> ignore

        let rng = System.Random()
        List.concat [
            for timeIndex in 1..1000 ->
            let date = DateTime.Now.AddMinutes(float timeIndex)
            [
                for varIndex in 0..4 ->
                { VarName = $"Var{varIndex % 5}"
                  Date = date
                  Value = rng.NextDouble() |> Convert.ToDecimal
                  Unit =  ["<kg>"; "<kg^2>" ; "<kg>"; "<m>"; "<s>"] |> List.item varIndex
                }
            ]
        ] |> List.map(fun msg -> client.Publish(msg.VarName, msg |> System.Text.Json.JsonSerializer.Serialize |> System.Text.Encoding.ASCII.GetBytes)) |> ignore

        client.Disconnect()
        "Ok"
