namespace Publisher

open System

type Ecuacion =
    { 
      Id_Ecuacion: int
      Formula: string
    }