namespace Publisher

open System

type Proceso =
    { 
      Nombre: string
      Descripcion: DateTime
      Ecuaciones: List<Ecuacion>
    }