namespace Publisher

open System

type Values =
    { 
      VarName: string
      Date: DateTime
      Value: decimal
      Unit: string
    }